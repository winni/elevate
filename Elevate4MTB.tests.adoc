= Tests for "Elevate4MTB.brf"
:hardbreaks:

== Bürgermeistersteg, Tongrube Marloffstein
[horizontal]
Avoid:: zu steiler Anstieg
Costs:: 9.113
Average Cost Factor::  1,98

https://brouter.de/brouter-web/#map=17/49.60327/11.02212/osm-mapnik-german_style,route-quality&lonlats=11.021335,49.602228;11.061114,49.62227[]

== Lindelberg, Igensdorf
[horizontal]
Require:: Am Waldrand
Costs::  7.145
Average Cost Factor::  1,89

https://brouter.de/brouter-web/#map=15/49.6291/11.1894/osm-mapnik-german_style,route-quality&lonlats=11.181075,49.625359;11.226322,49.621408[]

